package ldh.common.testui.model;

import lombok.Data;

@Data
public class DataExport {

    private Long id;
    private Integer treeNodeId;
    private Integer databaseParamId;
    private String dir;
    private String name;
    private String data;

}

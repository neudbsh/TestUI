package util;

import ldh.common.testui.util.JsonUtil;
import ldh.common.testui.vo.JsonPathHelp;
import org.junit.Test;

import java.util.Map;
import java.util.HashMap;

/**
 * Created by ldh on 2019/1/24.
 */
public class JsonPathHelpTest {

    @Test
    public void jsonTest() {
        Map<String, Object> map = new HashMap();
        map.put("result", 1);
        map.put("reason", null);
        map.put("success", true);

        Map<String, Object> content = new HashMap();
        content.put("assetId", 1);
        content.put("transactionId", "autotest_20190123181118");
        content.put("vendor", "BBNet");

        Map<String, Object> demo = new HashMap();
        demo.put("id", 1);
        content.put("demo", JsonUtil.toJson(demo));
        map.put("content", JsonUtil.toJson(content));

        String json = JsonUtil.toJson(map);

        System.out.println("json:" + json);
        String jsonPath = "Json.toJson(Json.toJson($.content).demo).id";
        JsonPathHelp jsonPathHelp = new JsonPathHelp(json, jsonPath);
        Integer assertId = jsonPathHelp.jsonValue(Integer.class);
        System.out.println("value:" + assertId);
    }
}
